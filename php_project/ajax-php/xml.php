<?php
$conn = mysqli_connect('localhost', 'root', '', 'demo_ajax') or die('Cannot connect to DB');

$query = mysqli_query($conn, 'SELECT * FROM member');

echo '<?xml version="1.0" encoding="UTF-8"?>';
echo '<root>';

if (mysqli_num_rows($query) > 0) {
  while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
    echo '<items>';
      echo '<username>'.$row['username'].'</username>';
      echo '<email>'.$row['email'].'</email>';
    echo '</items>';
  }
}
echo '</root>';
