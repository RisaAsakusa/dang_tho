<?php get_header(); ?>
    <div class="c-header__top">
      <div class="showPC">
        <div class="link1">
          <a class="icon1" href="#">
            掲載・取材依頼の企業様へ
            <img src="<?php echo get_template_directory_uri() . '/img/icon.png'; ?>" alt="icon.png">
          </a>
        </div>
      </div><!--end showPC-->
    </div><!--end-->

    <div class="l-header__main">
      <?php get_template_part("content", "menu"); ?>
    </div><!--end-->
  </div>
</header><!-- end c-header -->

<div class="c-mainVisual c-mainVisual--contact">
  <div class="c-banner1 c-banner1__contact">
    <div class="l-container">
      <div class="banner__box1">
        <div class="banner__img1">
          <img src="<?php echo get_template_directory_uri() .
           '/img/contact/CONTACt.png' ;?>" alt="CONTACt.png">
        </div>
      </div>
    </div><!--end l-container-->
  </div><!--end c-banner1-->
</div><!-- end c-mainVisual -->

<main class="l-main">
  <div class="l-container">
    <div class="c-breadcrumb">
      <div class="l-container">
        <a href="<?php echo get_home_url(); ?>">ホーム</a>
        <span>お問い合わせ</span>
      </div>
    </div><!--end breadcrumb-->

    <section class="p-contact1">
      <div class="p-contact1__inner">
        <div class="p-contact1__box1">
          <div class="p-contact1__title1">
            <h2>一般･学生の皆様へ</h2>
          </div>

          <div class="contact1__content1">
            <p>この度は愛媛シゴト図鑑をご覧いただきありがとうございます。<br>
              愛媛シゴト図鑑を見て疑問に思ったこと、ご質問等ございましたら下記の問い
              合わせフォームにて意見をお寄せください。少し聞きづらいなと思うようなこ
              とも遠慮なくご質問ください。愛媛シゴト図鑑は頑張る就活生を応援しています！<br>
              個人情報の取扱に関しましては、
              <span class="u-text__green">プライバシーポリシー</span>をご確認くだ
              さい。<br>
              <span class="u-text__red">※は入力必須です。</span>
              </p>
          </div>
        </div><!--end p-contact1__box1-->

        <div class="p-contact1__form1">
          <?php echo do_shortcode('[contact-form-7 id="391" title="contact-form"
          html_class= "c-form1"]'); ?>
        </div><!--end p-contact1__form1-->

        <div class="p-contact1__btn1">
          <div class="l-btn1">
            <div class="c-btn1">
              <a href="">この内容で送信する</a>
            </div>
          </div>
        </div>
      </div><!--end p-contact1__inner-->
    </section><!--end p-contact1-->
  </div><!-- end l-container-->
</main>

<?php get_footer(); ?>
