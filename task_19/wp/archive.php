<?php get_header(); ?>
    <div class="c-header__top">
      <div class="showPC">
        <div class="link1">
          <a class="icon1" href="#">
            掲載・取材依頼の企業様へ
            <img src="<?php echo get_template_directory_uri() . '/img/icon.png'; ?>" alt="icon.png">
          </a>
        </div>
      </div><!--end showPC-->
    </div><!--end-->

    <div class="l-header__main">
      <?php get_template_part("content", "menu"); ?>
    </div><!--end-->
  </div>
</header><!-- end c-header -->

<main class="l-main">
  <div class="l-container">
    <div class="c-breadcrumb">
      <div class="l-container">
        <a href="<?php echo get_home_url(); ?>">ホーム</a>
        <span>ニュース一覧</span>
      </div>
    </div><!--end breadcrumb-->

    <div class="p-news1">
      <div class="p-news1__inner">
        <?php $query = new WP_Query(array('post_type'=>'post', 
          'post_status'=>'publish', 'paged' => get_query_var( 'paged' )));
          if($query->have_posts()): ?>
          <ul class="c-listpost">
            <?php while($query->have_posts()) : $query->the_post(); ?>
            <li class="c-listpost__item">
              <span class="datepost"><?php echo get_the_date(" Y.m.d "); ?></span>
              <h3 class="c-listpost__title">
                <a href="<?php the_permalink();?>" title=""><?php the_title(); ?></a>
              </h3>
            </li>
            <?php endwhile; ?>
          </ul><!--end c-listpost-->

          <div class="p-news1__box1">
            <div class="c-pagination">
              <?php
                $html = paginate_links( array(
                  'paged' => ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1,
                  'total' => $query->max_num_pages,
                  'show_all' => true,
                  'prev_next' => true,
                  'prev_text' => __("PREV"),
                  'next_text' => __("NEXT")
                ));
                echo $html;
                wp_reset_postdata();
                ?>
            </div>
          </div>
          <?php else: ?>
            <?php _e('Sorry'); ?>
        <?php endif;?>
      </div><!--end p-news1__inner-->
    </div><!--end p-news1-->
  </div><!-- end l-container-->
</main><!-- end l-main -->
<?php get_footer(); ?>