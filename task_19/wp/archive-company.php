<?php get_header(); ?>
    <div class="c-header__top">
      <div class="showPC">
        <div class="link1">
          <a class="icon1" href="#">
            掲載・取材依頼の企業様へ
            <img src="<?php echo get_template_directory_uri() . '/img/icon.png'; ?>" alt="icon.png">
          </a>
        </div>
      </div><!--end showPC-->
    </div><!--end-->

    <div class="l-header__main">
      <?php get_template_part("content", "menu"); ?>
    </div><!--end-->
  </div>
</header><!-- end c-header -->

<div class="c-mainVisual c-mainVisual--company">
  <div class="c-banner1 c-banner1__company">
    <div class="l-container">
      <div class="banner__box1">
        <div class="banner__img1">
          <img src="<?php echo get_template_directory_uri() .
           '/img/company/CORPORATE-INFORMATION.png' ;?>" alt="CORPORATE-INFORMATION.png">
        </div>
      </div>
    </div><!--end l-container-->
  </div><!--end c-banner1-->
</div><!-- end c-mainVisual -->

<main class="l-main">
  <div class="l-container">
    <div class="c-breadcrumb">
      <div class="l-container">
        <a href="<?php echo get_home_url(); ?>">ホーム</a>
        <span>企業一覧</span>
      </div>
    </div><!--end breadcrumb-->

    <div class="p-company1">
      <div class="l-container">
        <div class="l-list1 l-list1__company1">
          <section class="c-list1">
            <?php $query = new WP_Query(array(
            'post_type'=>'company',
            'posts_per_page'=> 9,
            'post_status'=>array('future', 'publish'),
            'paged' => get_query_var('paged'))); ?>

            <?php if($query->have_posts()): ?>
            <?php while($query->have_posts()) : $query->the_post(); ?>
            <a class="c-list1__link1" href="<?php the_permalink(); ?>">
              <div class="c-list1__card1">
                <div class="card1__box1">
                  <div class="card1__img1">
                    <?php
                      // Post thumbnail.
                      the_post_thumbnail('full', array('class' => 'img-fluid rounded'));
                    ?>
                  </div>
                </div>

                <div class="card1__box2">
                  <h3><?php the_title(); ?></h3>
                  <span class="small-text1"><?php echo get_field("location"); ?></span>
  
                  <div class="card1__text1">
                    <div class="text1__inner">
                      <p><?php echo get_field("message"); ?></p>
                    </div>
                    <i class="img-icon1"><img src="<?php echo get_template_directory_uri() .
                    '/img/icon1.png'; ?>" alt=""></i>
                  </div>
  
                  <?php
                  $post_tags = get_the_tags();
                  if ( $post_tags ):?>
                    <span class="tag1"><?php echo $post_tags[0]->name; ?></span>
                  <?php endif; ?>
                </div>
              </div><!--end c-list1__card1-->
            </a>
            <?php endwhile; ?>
          </section><!--end c-list1-->
        </div>

        <div class="c-pagination">
          <?php
            pagination_cat_dangtho($query);
            wp_reset_postdata();
            ?>
        </div>
        <?php else: ?>
          <h1>Can't find post!</h1>
        <?php endif; ?>
      </div><!--end l-container-->
    </div><!--end p-company1-->
  </div><!-- end l-container-->
</main>

<?php get_footer(); ?>
