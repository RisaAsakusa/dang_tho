<div class="showSP">
  <nav class="c-menulistSP">
    <div class="c-menuSP c-menuSP-js">
      <div class="c-menuSP__1"></div>
      <div class="c-menuSP__2"></div>
      <div class="c-menuSP__3"></div>
      <div class="c-menuSP__title1">
        <p>閉じる</p>
      </div>
    </div>

    <ul class="mt1">
      <li><a href="<?php echo get_home_url(); ?>">ホーム</a></li>
      <li><a href="">企業一覧</a></li>
      <li><a href="">愛媛シゴト図鑑とは</a></li>
      <li><a href="<?php echo get_home_url() . '/interview'; ?>">インタビュー</a></li>
      <li><a href="<?php echo get_home_url() . '/news'; ?>">ニュース</a></li>
      <li><a href="">運営会社</a></li>
      <li><a href="">お問い合わせ</a></li>
    </ul>

    <div class="menulistSP__box1">
      <div class="box1__tel">
        <h2><span class="sm-text1">TEL.</span> 089-947-1411</h2>
      </div>

      <div class="box1__title1">
        <h3>受付時間9:00〜17:00（平日のみ）</h3>
      </div>
    </div>

    <div class="menulistSP__btn1">
      <div class="btn1__inner">
        <a href="#">掲載・取材のご依頼についてはこちら</a>
      </div>
    </div>

    <ul>
      <li><a href="#">プライバシーポリシー</a></li>
      <li><a href="#">利用規約</a></li>
      <li class="closeMenu closeMenu_js"><a href="#"><i class="fas fa-times"></i>閉じる</a></li>
    </ul>
  </nav><!--end c-menulistSP-->
</div><!--show menu in SP-->