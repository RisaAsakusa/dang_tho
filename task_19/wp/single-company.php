<?php get_header(); ?>
    <div class="c-header__top">
      <div class="showPC">
        <div class="link1">
          <a class="icon1" href="#">
            掲載・取材依頼の企業様へ
            <img src="<?php echo get_template_directory_uri() . '/img/icon.png'; ?>" alt="icon.png">
          </a>
        </div>
      </div><!--end showPC-->
    </div><!--end-->

    <div class="l-header__main">
      <?php get_template_part("content", "menu"); ?>
    </div><!--end-->
  </div>
</header><!-- end c-header -->

<div class="c-mainVisual c-mainVisual--company">
  <div class="c-banner1 c-banner1__company">
    <div class="l-container">
      <div class="banner__box1">
        <div class="banner__img1">
          <img src="<?php echo get_template_directory_uri() .
           '/img/company/CORPORATE-INFORMATION.png' ;?>" alt="CORPORATE-INFORMATION.png">
        </div>
      </div>
    </div><!--end l-container-->
  </div><!--end c-banner1-->
</div><!-- end c-mainVisual -->

<main class="l-main">
  <?php if(have_posts()): ?>
    <?php while(have_posts()) : the_post(); ?>
    <section class="p-comsingle1">
      <div class="l-container">
        <div class="c-breadcrumb">
          <div class="l-container">
            <a href="<?php echo get_home_url(); ?>">ホーム</a>
            <a href="<?php echo get_home_url() . "/company"; ?>">企業一覧</a>
            <span>企業一覧</span>
          </div>
        </div><!--end breadcrumb-->

        <div class="p-comsingle1__box1">
          <div class="comsingle1__box1">
            <div class="comsingle1__slider1 single-sider1_js">
              <?php
              $images = get_field("image");
              $size = 'full';

              if ($images):?>
                <?php foreach( $images as $image ): ?>

                <div class="custom__imgbox1">
                  <div class="custom__img1">
                    <?php echo wp_get_attachment_image( $image['ID'], $size); ?>
                  </div>
                </div>
                <?php endforeach; ?>
              <?php else: echo "?????"; ?>
              <?php endif; ?>
            </div>

            <div class="custom__title1">
              <?php
              $caption = get_field("caption");
              if ($images):
              ?>
              <h3><?php echo $caption ?></h3>
            <?php endif; ?>
            </div>
          </div>

          <div class="comsingle1__box2">
            <div class="custom__box1">
              <span class="datepost"><?php echo get_the_date("Y.m.d"); ?></span>

              <h2 class="custom__title1"><?php the_title(); ?></h2>

              <?php
              $location = get_field("location");
              $message = get_field("message");
              ?>
              <p class="custom__location1"><?php echo $location; ?></p>

              <div class="custom__mess1">
                <p><?php echo $message; ?></p>
              </div>
            </div>

            <?php
            $post_tags = get_the_tags();
            if ( $post_tags ):?>
              <span class="tag1"><?php echo $post_tags[0]->name; ?></span>
            <?php endif; ?>

            <div class="custom__content1">
              <?php the_content(); ?>
            </div>
          </div>
        </div>
      </div><!--end l-container-->
    </section><!--end p-comsingle1-->

    <section class="p-comsingle2">
      <div class="l-container">
        <div class="p-comsingle2__box1">
          <div class="comsingle2__data1">
            <h3 class="pos-1">企業DATA</h3>
          </div>

          <div class="comsingle2__content1">
            <div class="content1__row1">
              <?php
              $company = get_field("company_name");
              $link1 = get_field("url");?>

              <h4><?php echo $company; ?></h4>
              <a class="row1__link1" href=""><?php echo $link1; ?>
              <i class="fas fa-external-link-alt"></i></a>
            </div>

            <div class="content1__row2">
              <div class="row2__box1 row2--mr1">
                <div class="row2__form-group">
                  <label>本社</label>
                  <div class="form-info1"><?php echo get_field("address"); ?></div>
                </div>

                <div class="row2__form-group">
                  <label>従業員数</label>
                  <div class="form-info1"><?php echo get_field("number_employee"); ?>名</div>
                </div>

                <div class="row2__form-group">
                  <label>事業内容 </label>
                  <div class="form-info1"><?php echo get_field("business__content"); ?></div>
                </div>
              </div>

              <div class="row2__box2 row2--mr2">
                <div class="row2__form-group">
                  <label>TEL</label>
                  <div class="form-info1"><?php echo get_field("tel"); ?></div>
                </div>

                <div class="row2__form-group">
                  <label>代表者</label>
                  <div class="form-info1"><?php echo get_field("president"); ?></div>
                </div>
              </div>

              <div class="row2__box3 row2--mr3">
                <div class="row2__form-group">
                  <label>設立</label>
                  <div class="form-info1"><?php echo get_field("established"); ?></div>
                </div>
              </div>
            </div><!--end row2-->

            <div class="content1__row3">
              <div class="row3__form-group">
                <div class="title1">
                  <h2>新卒採用実績</h2>
                </div>

                <div class="content1">
                  <p><?php echo get_field("recruit"); ?></p>
                </div>
              </div>
            </div><!--end row3-->
          </div><!--end content1-->
        </div>
      </div><!--end l-container-->
    </section><!--end p-comsingle2-->

    <section class="p-comsingle3">
      <div class="p-comsingle3__inner">
        <div class="p-comsingle3__box1">
          <div class="c-title2">
            <h2>会社のココが良い！</h2>
          </div>

          <?php

          if(have_rows("strength")):

            while(have_rows("strength")): the_row();
              $title = get_sub_field('title');
              $content = get_sub_field('content');
          ?>

          <div class="comsingle3__strengh1">
            <h3><?php echo $title; ?></h3>
            <p><?php echo $content; ?></p>
          </div>
          <?php endwhile;?>
          <?php endif;?>
        </div>

        <div class="p-comsingle3__box2">
          <div class="c-title2">
            <h2>我が社の サービス</h2>
          </div>

          <div class="comsingle3__service1">
          <?php

          if(have_rows("service")):

            while(have_rows("service")): the_row();
              $image = get_sub_field('image');
              $content = get_sub_field('text');
              $size = 'full';
          ?>
            <div class="service1__box1">
              <div class="service1__img1">
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
              </div>

              <?php echo $content; ?>
            </div>
          <?php endwhile;?>
          <?php endif;?>
          </div>
        </div><!--end p-comsingle3__box2-->
      </div><!--end p-comsingle3__inner-->
    </section><!--end p-comsingle3-->

    <section class="p-comsingle4">
      <div class="l-container">
        <div class="p-comsingle4__title1">
          <div class="img1">
            <img src="<?php echo get_template_directory_uri() . '/img/company/shining.png'; ?>" alt="shining.png">
          </div>
          <div class="text1">
            <p>この企業に関連する記事のご紹介です。</p>
          </div>
        </div><!--end p-comsingle4__title1-->

        <div class="p-comsingle4__list1">
          <div class="l-list2">
            <div class="c-list2">

              <?php
              $current_title = get_the_title();
              $count = 0;
              $num = 1;

              $query = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page'=>-1,
                'post_status' => array('future', 'publish'),
                'paged' => get_query_var( 'paged' )));
              if($query->have_posts()): ?>

              <?php while($query->have_posts() && $count < 4) : $query->the_post();

                $similiar_post = get_the_tags()[0]->name;
                $num++;
                //echo $similiar_post;
                if (trim($current_title) == trim($similiar_post)):
                  $count++;
                ?>
                <a class="c-list2__link1" href="<?php the_permalink(); ?>">
                  <div class="c-list2__card1">
                    <div class="c-list2__img1">
                      <?php
                      $company_name = get_the_tags()[0]->name;
                      the_post_thumbnail('full', array('class' => 'img-fluid rounded'));
                      ?>
                    </div>
                    <div class="c-list2__text1">
                      <h2><?php the_title(); ?></h2>
                      <span class="company-name"><?php echo $company_name; ?></span>
                    </div>
                  </div><!--end c-list2__card1-->
                </a><!--end c-list2__link1-->
                <?php endif; ?>
              <?php endwhile; wp_reset_postdata();?>
              <?php else: echo '?'; ?>
                <?php _e('Sorry'); ?>
              <?php endif;?>
            </div><!--end c-list2-->
          </div>
        </div><!--end p-comsingle4__list1-->
      </div>
    </section><!--end p-comsingle4-->

    <section class="p-comsingle5">
      <div class="l-container">
        <div class="p-comsingle5__title1">
          <h2>他の企業を探す</h2>
        </div>

        <div class="p-comsingle5__list1">
          <section class="l-list1 l-list1__comsingle1">
            <div class="c-list1">
              <?php
              $query = new WP_Query(array('post_type'=>'company',
              'post_status'=>array('publish', 'future'), 'posts_per_page' => 3,
              'paged' => get_query_var( 'paged' )));
              if($query->have_posts()): ?>

              <?php while($query->have_posts()) : $query->the_post(); ?>
              <a class="c-list1__link1" href="<?php the_permalink(); ?>">
                <div class="c-list1__card1">
                  <div class="card1__box1">
                    <div class="card1__img1">
                      <?php
                        // Post thumbnail.
                        the_post_thumbnail('full', array('class' => 'img-fluid rounded'));
                      ?>
                    </div>
                  </div>

                  <div class="card1__box2">
                    <h2><?php the_title(); ?></h2>

                    <?php
                    $location1 = get_field("location");
                    $message1 = get_field("message");
                    ?>
                    <span class="small-text1"><?php echo $location1; ?></span>

                    <div class="card1__text1">
                      <div class="text1__inner">
                        <p><?php echo $message1; ?></p>
                      </div>
                      <i class="img-icon1"><img src="<?php echo get_template_directory_uri() .
                      '/img/icon1.png'; ?>" alt=""></i>
                    </div>

                    <span class="tag1">
                      <?php
                      $post1_tags = get_the_tags();
                      echo $post1_tags[0]->name;
                      ?>
                    </span>
                  </div>
                </div><!--end c-list1__card1-->
              </a>
              <?php endwhile; wp_reset_postdata();?>
              <?php else: ?>
                <?php _e('Sorry'); ?>
              <?php endif;?>
            </div><!--end list1-->
          </section>
        </div><!--end p-comsingle5__list1-->

        <div class="l-btn1">
          <div class="c-btn1">
            <a href="<?php echo get_home_url() . '/company'; ?>">企業一覧はこちら</a>
          </div>
        </div>
      </div><!--end l-container-->
    </section><!--end p-comsingle5-->
    <?php endwhile; ?>
  <?php else: ?>
    <h1>Can't find post!</h1>
  <?php endif; ?>
</main>

<?php get_footer(); ?>
