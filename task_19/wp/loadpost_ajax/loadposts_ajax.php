<?php
//load posts by Ajax
if (!function_exists("getPost")) {
  function getPost() {
    if(!isset($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'bobz')) {
      die('Permission denied!');
      $response = [
        'status'  => 500,
        'message' => 'Something is wrong, please try again later ...',
        'content' => false,
        'found'   => 0
      ];
    }

    $tax = sanitize_text_field($_POST['params']['tax']);
    $term = $_POST['params']['term'];
    $page = intval($_POST['params']['page']);
    $qty  = intval($_POST['params']['qty']);

    if (!term_exists($term, $tax) && $term != 'all-terms'):
      $response = [
        'status'  => 501,
        'message' => 'Term doesn\'t exist:' . $term,
        'content' => 0
      ];
      die(json_encode($response));
    endif;

    if ($term == 'all-terms'):
      $tax_qry[] = [
        'post_type'=>'interview',
        'post_status'=>array('publish', 'future')
      ];
    else :
      $tax_qry[] = [
        'taxonomy' => $tax,
        'field'    => 'slug',
        'terms'    => $term,
      ];
    endif;

    $args = [
      'paged'          => $page,
      'post_type'      => 'interview',
      'post_status'    => array('publish', 'future'),
      'posts_per_page' => $qty,
      'tax_query'      => $tax_qry
    ];
    $qry = new WP_Query($args);

    ob_start();
    if ($qry->have_posts()) :
      while ($qry->have_posts()) : $qry->the_post(); ?>
        <div class="c-card1">
          <div class="c-card1__box1">
            <div class="c-card1__img1">
              <?php
                // Post thumbnail.
                the_post_thumbnail('full', array('class' => 'img-fluid rounded'));
              ?>
            </div>
            <a class="small-title1"
            href="<?php addLinkCategory_dangtho(get_the_category( $id )[0]->name); ?>">
              <p><?php echo get_the_category( $id )[0]->name; ?></p>
            </a>
          </div>

          <div class="c-card1__text1">
            <?php
            $message = get_field('message');
            if($message):
            ?>
            <a class="card1__title1 card1__link1" href="<?php the_permalink(); ?>">
              <?php echo $message['title1']; ?>
              <i class="img-icon1"><img src="<?php echo get_template_directory_uri() . 
              '/img/icon1.png'; ?>" alt="icon1"></i>
            </a>

            <p><?php echo $message['text1']; ?></p>
            <?php endif; ?>
          </div>
        </div>
      <?php endwhile;?>

        <div class="c-pagination">
          <?php
            $html = paginate_links( array(
              'paged' => ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1,
              'total' => $query->max_num_pages,
              'show_all' => false,
              'prev_next' => true,
              'prev_text' => __("PREV"),
              'next_text' => __("NEXT")
            ));
            echo $html;
            wp_reset_postdata();
            ?>
        </div>
    <?php
      $response = [
        'status'=> 200,
        'found' => $qry->found_posts
      ];
    else:
      $response = [
        'status'  => 201,
        'message' => 'No posts found ' . $term
      ];
    endif;

    $response['content'] = ob_get_clean();
    die(json_encode($response));
  }
}

add_action('wp_ajax_do_filter_posts', 'getPost');

//handling AJAX requests from unauthenticated users
add_action('wp_ajax_nopriv_do_filter_posts', 'getPost');

/*
@create shortcode for pagination
*/
if (!function_exists("createShortCode")) {
  function createShortCode($atts) {
    $status = shortcode_atts( array(
      'tax'      => 'category',
      'terms'    => false,
      'active'   => false
    ), $atts );

    $result = NULL;
    $terms  = get_terms($status['tax']);
    if(count($terms)):
      ob_start(); ?>

      <div id="container-async" class="p-interview1__listbtn1 sc-ajax-filter"
      data-paged="<?php echo "9"; ?>">
        <div class="btn__tab1">
          <a href="#" data-filter="<?php echo 'all' ?>" data-term="all-terms" data-page="1">すべて</a>
        </div>

        <?php foreach ($terms as $term) : ?>
        <div class="btn__tab1 <?php if ($term->term_id == $status['active']) :?> active<?php endif; ?>">
          <a href="<?php echo get_term_link( $term, $term->taxonomy ); ?>" data-filter="<?php echo $term->taxonomy; ?>" data-term="<?php echo $term->slug ?>" data-page="1"><?php echo $term->name; ?></a>
        </div>
        <?php endforeach; ?>
      </div>

      <div class="p-interview1__tab1 tabs_js active" id="all">
        <div class="p-interview1__list1">
        </div><!--end p-interview1__list-->
      </div><!--end tab1-->

      <!-- <?php //pagination_tab($qry,$page); ?> -->
    <?php $result = ob_get_clean();
    endif;
    return $result;
  }
}
add_shortcode('multitab_ajax', 'createShortCode');

function pagination_tab( $query = null, $paged = 1 ) {
  if (!$query)
    echo '??????';
    return;
  $paginate = paginate_links([
    'base'      => '%_%',
    'total'     => $query->max_num_pages,
    'format'    => '#page=%#%',
    'current'   => max( 1, $paged ),
    'prev_next' => true,
    'prev_text' => __("PREV"),
    'next_text' => __("NEXT")
  ]);
  //if ($query->max_num_pages > 1) : ?>
    <div class="c-pagination">
      <?php foreach ( $paginate as $page ) :?>
        <?php echo $page; ?>
      <?php endforeach; ?>
    </div>
    <?php //else: echo $query->max_num_pages . "dasdasd"; ?>
  <?php //endif;
}
