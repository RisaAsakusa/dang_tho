<?php get_header(); ?>
    <div class="c-header__top">
      <div class="showPC">
        <div class="link1">
          <a class="icon1" href="#">
            掲載・取材依頼の企業様へ
            <img src="<?php echo get_template_directory_uri() . '/img/icon.png'; ?>" alt="icon.png">
          </a>
        </div>
      </div><!--end showPC-->
    </div><!--end-->

    <div class="l-header__main l-header__main--news">
      <?php get_template_part("content", "menu"); ?>
    </div><!--end-->
  </div>
</header><!-- end c-header -->
<div class="l-container">
  <?php
    _e('<h2>404 NOT FOUND</h2>');
    _e('<p>The article you were looking for was not found, but maybe try looking again!</p>');
  ?>
</div>
<?php get_footer(); ?>
