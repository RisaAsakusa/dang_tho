<?php
/*
*add theme_support(menu, thumbnail, ....) for wordpress;
*/
add_theme_support( 'post-thumbnails' );
add_theme_support( 'menus' );

/*
*function remove amrgin-top: 31px !important;
*/
function remove_admin_login_header() {
  remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');


/*
*add boostrap link and file js, css
*/
function reg_scripts() {
  wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', array(), '1.0.0', 'all' );
  wp_enqueue_style( 'bootstrapthemestyle', get_template_directory_uri() . '/css/vendor/bootstrap/css/bootstrap.min.css', array(), '1.0.0', 'all' );
  wp_enqueue_script( 'bootstrap-script1', get_template_directory_uri() . '/css/vendor/jquery/jquery.min.js',
    array(), true);
  wp_enqueue_script( 'bootstrap-script2', get_template_directory_uri() . '/css/vendor/bootstrap/js/bootstrap.bundle.min.js',
    array(), true);
}
add_action( 'wp_enqueue_scripts', 'reg_scripts' );

/*
*Register right sidebar
*/
function sidebar_unit() {
  register_sidebar( array(
    'name' => __('Right Hand Sidebar'),
    'id' => 'right-sidebar',
    'description' => __('Widgets in this area will be shown on the right-hand side'),
    'before_title' => '<h5 class="card-header">',
    'after_title' => '</h5>',
    'before_widget' => '<div id="%1$s" class="card my-4">',
    'after_widget' => '</div><!-- widget end -->'
  ));
}
add_action('widgets_init', "sidebar_unit");

/*
*add class css to link menu
*/
function menuAddClass( $atts, $item, $args ) {
  $class = 'nav-link'; //or something based on $item
  $atts['class'] = $class;
  return $atts;
}
add_filter( 'nav_menu_link_attributes', 'menuAddclass', 10, 3 );

/*
*add class css page-link to post-link(pagination)
*/
function posts_link_attributes() {
  return 'class="page-link"';
}
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

/*
*pagination
*/
if (!function_exists('pagination_nav')) {
  function pagination_nav() {
  global $wp_query;

  if ( $wp_query->max_num_pages <= 2 ) { ?>
    <ul class="pagination justify-content-center mb-4" role="navigation">
      <?php
        //if previous posts pages link existed, web will show a link name '← Older'.
        if(get_previous_posts_link( '← Older')!==null) {?>
          <li class="page-item">
            <?php previous_posts_link( '← Older'); ?>
          </li><!-- end page-item -->
        <?php
        } else
        //if no previous posts pages link, web will show a link '← Older' disabled.
        {?>
          <li class="page-item disabled">
            <?php echo '<a class="page-link" href="" title="">← Older</a>'; ?>
          </li><!-- end page-item -->
          <?php
        }
        //if next posts pages link existed, web will show a link name 'Newer →'.
        if(get_next_posts_link( 'Newer →')!==null) {?>
          <li class="page-item">
            <?php next_posts_link( 'Newer →'); ?>
          </li><!-- end page-item -->
        <?php
        } else
        //if next posts pages link existed, web will show a link name 'Newer →' disabled.
        {?>
          <li class="page-item disabled">
            <?php echo '<a class="page-link" href="" title="">Newer →</a>'; ?>
          </li><!-- end page-item -->
          <?php
        }
      ?>
    </ul>
    <!-- ---------------------------------------- -->
  <?php }
  //-------------------------------------------------
  }
  //-------------------------------------------------
}

//custom post type
function createCustomPostType() {
  //$label uses for contain text which relative to the name
  //of post type
  $label = array(
    'name' => 'products',
    'singular_name' => 'product'
  );

  $arr = array(
    //variable config custom post type
    'labels' => $label,
    'description' => 'Post type up product',
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'author',
      'thumbnail',
      'comments',
      'trackbacks',
      'revisions',
      'custom-fields'
    ), //features are supported in post type
    //taxonomys be admit to separate contents
    'taxonomies' => array('category', 'post_tag'),
    'hierarchical' => false, //allow decentralization, if false => like post, true => like page
    'public' => true, //activated post type
    'show_ui' => true, //show manager box like Post/Page
    'show_in_menu' => true, //show on Admin menu(left)
    'show_in_nav_menus' => true, //show on Appearance -> Menus
    'show_in_admin_bar' => true, //show on Admin bar(black)
    'menu_position' => 5, //order show in menu(left)
    'menu_icon' => '',
    'can_export' => true, //Can export content by Tools -> Export
    'has_archive' => true, //allow store (month, date, year)
    'exclude_from_search' => false, //delete from search result
    'publicly_queryable' => true, //Show parameters on query, must to set true.
    'capability_type' => 'post' //
  );

  //slug post type is very important, you can set free
  //but it have not space, special char,...
  register_post_type('product', $arr);
}
add_action('init', 'createCustomPostType');

//show custom post type in Index.html
add_filter('pre_get_posts', 'getCustomPostType');
function getCustomPostType($query) {
  if (is_home() && $query -> is_main_query())
    $query->set ('post_type', array('post', 'product'));
  return $query;
}

$new_query = new WP_query( 'post_type="product"' );

