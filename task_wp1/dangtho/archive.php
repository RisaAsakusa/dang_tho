<?php 
//main template use for taxonomy
?>

<div class="archive-title">
  <h2>
    <?php 
      if (is_tag()) :
        printf(__('Posts Tagged: %1$s', 'dangtho'), single_tag_title('', false));
      elseif (is_category())  :
        printf(__('Posts Categorized: %1$s'), single_cat_title( '', false));
      elseif(is_day()) :
        printf(__('Daily Archives: %1$s'), get_the_time('l, F j, Y'));
      elseif ( is_month() ) :
        printf( __('Monthly Archives: %1$s','thachpham'), get_the_time('F Y') );
      elseif ( is_year() ) :
        printf( __('Yearly Archives: %1$s','thachpham'), get_the_time('Y') );
      endif;
    ?>
  </h2>
</div>

<?php if (is_tag() || is_category()) : ?>
  <div class="archive-dexcription">
    <?php echo term_description(); //return description of a taxonomy?>
  </div>
<?php endif; ?>
