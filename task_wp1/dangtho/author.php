<?php get_header(); ?>
  <h1 class="my-4">Page Heading
    <small>Secondary Text</small>
  </h1><!-- end my-4 -->
  <section id="main-content">
    <div class="author-box">
      <?php 
        //show avatar of author
        echo '<div class="author-avatar">'.get_avatar(get_the_author_meta('ID')).'</div>';
        //show author name
        printf('<h3>'. __('Posts by %1$s').'</h3>', get_the_author());
        //Introduce author
        echo '<p>' .get_the_author_meta('description') . '</p>';

        if ( get_the_author_meta( 'user_url' ) ) : printf( __('<a href="%1$s" title="Visit to %2$s website">Visit to my website</a>'),
                get_the_author_meta( 'user_url' ), get_the_author() );
        endif;
      ?>
    </div>
  </section>
  <?php if(have_posts()): ?>
    <?php while(have_posts()) : the_post(); ?>
      <!-- Blog Post -->
      <div class="card mb-4">
        <?php the_post_thumbnail('full', array('class' => 'card-img-top')); ?>
        <div class="card-body">
          <h2 class="card-title"><?php the_title(); ?></h2>
          <?php the_excerpt(); ?>
          <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More &rarr;</a>
        </div> <!-- end card-body -->
        <div class="card-footer text-muted">
          Posted on <?php the_time('F jS, Y'); ?> by
          <a href="#"><?php the_author_posts_link(); ?></a>
        </div> <!-- end card-footer -->
      </div> <!-- end card -->
    <?php endwhile; ?>
  <?php else: ?>
  <?php _e('Sorry'); ?>
  <?php endif; ?>
  <?php pagination_nav(); ?>
  </div>
<!-- Sidebar Widgets Column -->
<div class="col-md-4">
  <?php dynamic_sidebar('right-sidebar'); ?>
</div>
<?php get_footer(); ?>
