<!--► Comment Block ◄-->
<!--============================-->
<div class="l-comment">
<?php if ( have_comments() ) : ?>
<h2>
<?php comments_number('No', 'One', '%'); ?> thoughts on this awesome post !
</h2>
<?php
	wp_list_comments(
		array(
			'callback'		=> 'x_comments',
			'style'			=> 'ol',
			//'per_page'		=> '3',
			'short_ping' => true,
		)
	);
?>

<!--============================-->




<!--► Comments Pagination ◄-->
<!--============================-->
<div class="c-pagination e-goto_url">

<?php paginate_comments_links() ?>

</div><!-- ▲ .c-pagination ▲ -->
<!--============================-->

<hr class="c-misc_hr">

<!--► Post Comment ◄-->
<!--============================-->
<?php
$comments_arg = array(
	'fields' => apply_filters( 'comment_form_default_fields',
		array(
			'author'	=>	'<input id="author" name="author" type="text" placeholder="Username">',

			'email' 	=>	'<input type="email" id="email" name="email" type="text" placeholder="E-mail">' )
	),

	'comment_field'	=>	'<div class="c-post_comment--area">' .
						'<textarea id="comment" name="comment" aria-required="true" placeholder="Leave your awesome comment here !" required minlength="50" maxlength="500"></textarea>' .
						'<div>
						<a>50 &#60;<span>&#160; 0 &#160;</span>&#60; 500</a>
						</div>'.
						'</div>',

	'logged_in_as'	=> '<div class="c-post_comment">' .
	sprintf(
		__( '	<a class="c-post_comment--as">
		<span>Logged in as</span>
		<span>%2$s</span>.
		<a href="%3$s" class="c-post_comment--logout">Log out</a>' ),

		admin_url( 'profile.php' ),
		$user_identity,
		wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
	) . '</div>',

	'class_form'			=> 'l-post_comment',
	'title_reply_before'	=> '',
	'comment_notes_after'	=> '',
	'title_reply'			=> '',
	'title_reply_to'		=> 'Salty Reply',
	'cancel_reply_link'		=> 'Cancel',
	'class_submit'			=> 'c-post_comment--post',
	'label_submit'			=> 'Post Comment',
	'comment_notes_before'	=> ''
);

comment_form($comments_arg);

?>

<!--============================-->

<?php endif; ?>
</div><!-- ▲ Comment Block ▲ -->
<!--============================-->