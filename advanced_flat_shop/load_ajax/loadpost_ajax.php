<?php
/*
*@title: Load post by AJAX
*@author: Huy san
*/
/*--------------------------------------*/

/*---------------------------------------
*action: get posts following term's name of each taxonomy type
*result: return required posts
---------------------------------------*/
if (!function_exists("getPost")) {
  function getPost() {
    //If have any security issue, disconnect to server and send message
    if (!isset($_GET['nonce']) || !wp_verify_nonce($_GET['nonce'], 'bobaz')){
      die('Permission denied!');
    }

    /*-------------------VARIABLE------------------*/
    $num_posts = $_GET['parameter']['posts']; //the number of posts when scrolling

    //name of taxonomy type(tag, category, etc.)
    $tax = $_GET['parameter']['tax'];

    //name of an item in the taxonomy type
    //(ex: 'demo1' in category, 'demo2' in tag, etc.)
    $term_name = $_GET['parameter']['name'];

    $arr = array(
      'post_type' => 'post',
      'post_status' => 'public',
      "posts_per_page" => $num_posts
    ); //array contain custom attributes to get posts

    $arr = addTermNameFollowTaxType($arr, $tax, $term_name);
    $wp_query = new WP_Query($arr);
    /*--------------------------------------------*/

    ob_start();
    if ($wp_query->have_posts()):
      while($wp_query->have_posts()) : $wp_query->the_post();
        get_template_part('content', 'post');?>
      <?php endwhile;
      wp_reset_postdata();
    endif;

    $response['content'] = ob_get_clean();
    die(json_encode( $response ));
  }
}
add_action('wp_ajax_do_filter_posts', 'getPost');

//handling AJAX requests from unauthenticated users
add_action('wp_ajax_nopriv_do_filter_posts', 'getPost');

/*-------------------------------------*/

/*---------------------------------------
*action: get posts is being searching in search form
*result: return required posts
---------------------------------------*/
if (!function_exists("getPostsWhenSearching")) {
  function getPostsWhenSearching() {
    //If have any security issue, disconnect to server and send message
    if (!isset($_GET['nonce']) || !wp_verify_nonce($_GET['nonce'], 'bobaz')){
      die('Permission denied!');
    }

    /*-------------------VARIABLE------------------*/
    $num_posts = $_GET['parameter']['posts']; //the number of posts when scrolling

    $search = $_GET['parameter']['search'];

    $arr = array(
      'post_type' => 'post',
      'post_status' => 'public',
      "posts_per_page" => $num_posts,
      's' => $search
    ); //array contain custom attributes to get posts
    
    $wp_query = new WP_Query($arr); //get posts according to above condition
    /*--------------------------------------------*/

    //return data to client's browser
    ob_start();
    if ($wp_query->have_posts()):
      while($wp_query->have_posts()) : $wp_query->the_post();
        get_template_part('content', 'post');
        ?>
      <?php endwhile;
      wp_reset_postdata();
    else: ?>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.·:::::::::::::::::·.&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.·´::::::::::::::::::::::`·.&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.·´:::.··.:::::`·.&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.·´::::.·´&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...&nbsp;&nbsp;&nbsp;&nbsp;`·.:::::`·.&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.·´::::.·´&nbsp;&nbsp;&nbsp;&nbsp;.´&nbsp;.··.&nbsp;`..´&nbsp;.··.&nbsp;`.&nbsp;&nbsp;&nbsp;`·.:::::`·.&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,'::::::,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.'&nbsp;&nbsp;,':::'|&nbsp;&nbsp;:&nbsp;&nbsp;'|:::',&nbsp;&nbsp;'.&nbsp;&nbsp;&nbsp;&nbsp;',:::::::`,&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,':::::::,'&nbsp;&nbsp;&nbsp;&nbsp;.'&nbsp;&nbsp;/::::::|&nbsp;&nbsp;'&nbsp;&nbsp;'|::::::\&nbsp;&nbsp;'.&nbsp;&nbsp;&nbsp;&nbsp;',:::::::',&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.·´:::::::·´&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/::::::::|&nbsp;&nbsp;&nbsp;&nbsp;'|:::::::'\&nbsp;&nbsp;`.&nbsp;&nbsp;&nbsp;`·.::::::`·.&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;!:::::::::!&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,·'::::::::,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',:::::::::'·,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;!::::::::!&nbsp;<br>
      &nbsp;&nbsp;&nbsp;.·´::::::::·´&nbsp;.´&nbsp;&nbsp;,'::::::::::,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',:::::::::::',&nbsp;&nbsp;`.&nbsp;`·.::::::`·.&nbsp;<br>
      &nbsp;&nbsp;!::::::::::!&nbsp;&nbsp;'.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',::::::::,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;',:::::::::.,'&nbsp;&nbsp;&nbsp;.'&nbsp;&nbsp;&nbsp;!::::::::'!&nbsp;<br>
      &nbsp;&nbsp;&nbsp;\::::::::::\&nbsp;&nbsp;`&nbsp;.&nbsp;&nbsp;&nbsp;&nbsp;'··´&nbsp;&nbsp;&nbsp;´&nbsp;.··.`&nbsp;&nbsp;&nbsp;`··'&nbsp;&nbsp;.&nbsp;´&nbsp;&nbsp;&nbsp;&nbsp;/::::::::/&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\:::::::::\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`·.___.·´&nbsp;&nbsp;,'::::',`·.____.·´&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/::::::::/&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',:::::::',&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,'::::::::',`.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,'::::::::,'&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;!::::::::!&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'·-·´¯`·-·'·'&nbsp;`:,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;!:::::::::!&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;!::::::::!&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;&nbsp;.··.&nbsp;&nbsp;&nbsp;&nbsp;::,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;!:::::::::!&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',:::::::',&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,´::::::::::::`,&nbsp;&nbsp;':::&nbsp;&nbsp;&nbsp;&nbsp;,'::::::::,'&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',:::::::',.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;;&nbsp;|:::::::::::::::|&nbsp;;:::&nbsp;&nbsp;&nbsp;,'|:::::::,'&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,´:::::::,´:`·.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;;&nbsp;|:::::::::::::::|&nbsp;;::&nbsp;&nbsp;&nbsp;,`'|::::::::`,&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',::::::',:::::',&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'|:::::::::::::::|&nbsp;;:&nbsp;&nbsp;&nbsp;,':,'::::::::,'&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;!:::::::!:::::',&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|:::::::::::::::|&nbsp;;·&nbsp;,':::!:::::::::!&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;!:::::::!::::::\&nbsp;&nbsp;&nbsp;&nbsp;'|:::::::::::::::|&nbsp;|&nbsp;&nbsp;.':::!:::::::::!&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/:::::::/::::::::\&nbsp;&nbsp;&nbsp;'',:::::::::::::,'&nbsp;'&nbsp;,'::::/::::::::'/&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/:::::::/:::::::::::'.&nbsp;&nbsp;&nbsp;',:::::::::::,'&nbsp;.':::::/::::::::'/&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;!::::::::!:::::::::::::'.&nbsp;&nbsp;',:::::::::,'&nbsp;.':::::!:::::::::!&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;!::::::::!:::::::::::::::&nbsp;&nbsp;',:::::::,'&nbsp;.'::::::i:::::::::!&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;,'::::::::,'::::::::::::::::`.&nbsp;`·-·´.':::::::,'::::::::,'&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;!:::::::::!::::::::::::::::::`·--·´::::::::!:::::::::!&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\::::::::\::::::::::::::::::::::::::::::::::/::::::::'/&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;!::::::::!:::::::::::::::::::::::::::::::::!:::::::::!*SouM*&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;!::::::::!'¯`·´¯`·.(¯`·´¯`·.(¯¯¯`·.|¯'!:::::::::!&nbsp;*GmA*&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\:::.·´&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\:::::.·´'*Cy&nbsp;TeK*&nbsp;<br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'\:'DaiR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\.·´&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*In&nbsp;'98*«</p>
    <?php endif;
    
    //get the current buffer content and delete current ouput buffer
    $response['content'] = ob_get_clean();
    die(json_encode( $response ));
  }
}
add_action('wp_ajax_do_filter_search_posts', 'getPostsWhenSearching');

//handling AJAX requests from unauthenticated users
add_action('wp_ajax_nopriv_do_filter_search_posts', 'getPostsWhenSearching');

/*-------------------------------------*/

/**-------------------------------------
 * ------------SHORTCODE----------------
--------------------------------------*/
/*--------------------------------------
*action: create shortcode use for load posts
*when scrolling page
*result: return html code when use this shorcode
*(ex: [loadPostWhenScroll tax="cat1"])
--------------------------------------*/
if(!function_exists("createShortCode")) {
  function createShortCode($atts) {
    $status = shortcode_atts(array(
      'tax' => 'category',
      'name' => ''
    ), $atts);

    $result = NULL;
    ob_start();?>
    <div class="l-block_card l-block_card_js"
      data-tax="<?php echo $status['tax'] ?>"
      data-name="<?php echo $status['name'] ?>">
    </div>
    <?php $result = ob_get_clean();
    return $result;
  }
}
add_shortcode( "loadPosts", 'createShortCode' );

/*-------------------------------------*/

/*---------------------------------------
*action: create shortcode use for load posts which
*is being searching when scrolling page
*result: return html code when use this shorcode
---------------------------------------*/
if(!function_exists("createShortCodeForSearchPost")) {
  function createShortCodeForSearchPost($atts) {
    $status = shortcode_atts(array(
      'search' => ''
    ), $atts);

    $result = NULL;
    ob_start();?>
    <div class="l-block_card search-posts_js"
      data-search="<?php echo esc_html($status['search']);?>">
    </div>
    <?php $result = ob_get_clean();
    return $result;
  }
}
add_shortcode( "loadPostsIsBeingSearched", 'createShortCodeForSearchPost' );

/*------------------------------------*/

/**-------------------------------------
 * ----------SUPPORT FUNCTION-----------
--------------------------------------*/
/*--------------------------------------
*action: add slug name following the type of
*taxonomy(category, tag,...)
*return: new array which is added new item
--------------------------------------*/
if(!function_exists("addTermNameFollowTaxType")) {
  function addTermNameFollowTaxType($arr, $tax, $name) {
    if ($tax == 'category') {
      $arr["category_name"] = $name;
    } else if($tax == 'post_tag') {
      $arr["tag"] = $name;
    }
    return $arr;
  }
}
