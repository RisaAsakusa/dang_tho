<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<!-- SECTION | S I D E B A R-->
<!---->
<!--		@sidebar-->
<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<div class="l-sidebar">

<?php get_search_form(); ?>

<?php if ( is_single() ) : ?>
<!--► Author ◄-->
<!--────────────────────────────────-->
<div class="c-author c-side">

	<h4>Written by</h4>

	<div>

		<?php echo get_avatar( get_the_author_meta( 'ID' ), 40 ); ?>

		<span><?php the_author_meta('display_name'); ?></span>

	</div>

</div><!-- ▲ c-author ▲ -->
<!--────────────────────────────────-->


<!--► Show Comments ◄-->
<!--────────────────────────────────-->
<?php
	if ( comments_open() ) :
	echo '<div class="c-show_comment c-side">Show Comments >></div>';
	endif;
?>

<!--────────────────────────────────-->


<!--► Category ◄-->
<!--────────────────────────────────-->
<div class="c-list e-goto_url c-side">

	<span>Post Categories</span>

	<?php the_category(); ?>

</div><!-- ▲ Category ▲ -->
<!--────────────────────────────────-->


<!--► Tag ◄-->
<!--────────────────────────────────-->
<div class="c-list e-goto_url c-side">

	<span>Post Tags</span>

	<?php the_tags( '<ul><li>', '</li><li>', '</li></ul>' ); ?>

</div><!-- ▲ Tag ▲ -->
<!--────────────────────────────────-->
<?php else : ?>


<!--► Category ◄-->
<!--────────────────────────────────-->
<div class="c-list e-goto_url c-side">

	<span>Categories</span>

	<ul>
		<?php wp_list_categories( array(
		'title_li'		=> '',
		'orderby'		=> 'name',
		'show_count'	=> true,
		//'exclude'		=> array( 10 )
		) ); ?>
	</ul>

</div><!-- ▲ Category ▲ -->
<!--────────────────────────────────-->


<!--► Tag ◄-->
<!--────────────────────────────────-->
<div class="c-list e-goto_url c-side">

	<span>Tags</span>

	<?php
		$tags = get_tags(array(
			'hide_empty' => false
		));
			echo '<ul>';
		foreach ($tags as $tag) {
			$tag_link = get_tag_link( $tag->term_id );
			echo "<li><a href='{$tag_link}'>" . $tag->name . '<span>(' . $tag->count . ')</span></a></li>';
		}
			echo '</ul>';
	?>

</div><!-- ▲ Tag ▲ -->
<!--────────────────────────────────-->
<?php endif; ?>


</div><!-- !SECTION | S I D E B A R-->
<!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->