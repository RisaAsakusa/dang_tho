<?php get_header(); ?>

<!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->
<!---->
<!-- ANCHOR | M A I N-->
<!---->
<!--		@main-->
<!---->
<!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->

<main class="l-main">


<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<!-- SECTION | S E C T I O N    B A N N E R -->
<!---->
<!--		@secbanner    @banner -->
<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<section class="l-banner">

	<div class="c-banner">
		<h2>Banner</h2>
	</div>

</section><!-- !SECTION | S E C T I O N    B A N N E R -->
<!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->







<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<!-- SECTION | S E C T I O N    1-->
<!---->
<!--		@sec1-->
<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<section class="p-index_1">
<div class="l-container">


	<!--► Title ◄-->
	<!--============================-->
	<h2 class="c-title">Feature Products</h2>
	<!--============================-->


	<!--► Product List ◄-->
	<!--============================-->
	<div class="l-block_card l-block_card--big">


		<!--► Card ◄-->
		<!--────────────────────────────────-->
		<div class="c-card e-goto_url">

			<!--► Image ◄-->
			<div class="c-card--img">
				<img alt="" width="220" height="220" src="https://picsum.photos/441/441/?random">
			</div>

			<!--► Product name ◄-->
			<h3 class="c-card--name">Beef</h3>

			<!--► Detail icon ◄--><a class="c-card--detail" href="product.html"><i class="fas fa-search"></i></a>

			<!--► Sale ◄-->
			<div class="c-card--sale">Sale !</div>

			<!--► Price ◄-->
			<span class="c-card--price">
				<del>$100</del>
				<ins>$80</ins>
			</span>

			<!--► Cart icon ◄-->
			<div class="c-card--cart"><i class="fas fa-shopping-cart"></i></div>

			<!--► Links ◄-->
			<a class="c-card--link" href="product.html"></a>

		</div><!-- ▲ c-card ▲ -->
		<!--────────────────────────────────-->


		<!--► Card ◄-->
		<!--────────────────────────────────-->
		<div class="c-card e-goto_url">

			<!--► Image ◄-->
			<div class="c-card--img">
				<img alt="" width="220" height="220" src="https://picsum.photos/442/442/?random">
			</div>

			<!--► Product name ◄-->
			<h3 class="c-card--name">Steak</h3>

			<!--► Detail icon ◄--><a class="c-card--detail" href="product.html"><i class="fas fa-search"></i></a>

			<!--► Sale ◄-->
			<div class="c-card--sale">Sale !</div>

			<!--► Price ◄-->
			<span class="c-card--price">
				<del>$50</del>
				<ins>$40</ins>
			</span>

			<!--► Cart icon ◄-->
			<div class="c-card--cart"><i class="fas fa-shopping-cart"></i></div>

			<!--► Links ◄-->
			<a class="c-card--link" href="product.html"></a>

		</div><!-- ▲ c-card ▲ -->
		<!--────────────────────────────────-->


		<!--► Card ◄-->
		<!--────────────────────────────────-->
		<div class="c-card e-goto_url">

			<!--► Image ◄-->
			<div class="c-card--img">
				<img alt="" width="220" height="220" src="https://picsum.photos/443/443/?random">
			</div>

			<!--► Product name ◄-->
			<h3 class="c-card--name">Jerk</h3>

			<!--► Detail icon ◄--><a class="c-card--detail" href="product.html"><i class="fas fa-search"></i></a>

			<!--► Sale ◄-->
			<div class="c-card--sale">Sale !</div>

			<!--► Price ◄-->
			<span class="c-card--price">
				<del>$60</del>
				<ins>$20</ins>
			</span>

			<!--► Cart icon ◄-->
			<div class="c-card--cart"><i class="fas fa-shopping-cart"></i></div>

			<!--► Links ◄-->
			<a class="c-card--link" href="product.html"></a>

		</div><!-- ▲ c-card ▲ -->
		<!--────────────────────────────────-->


		<!--► Card ◄-->
		<!--────────────────────────────────-->
		<div class="c-card e-goto_url">

			<!--► Image ◄-->
			<div class="c-card--img">
				<img alt="" width="220" height="220" src="https://picsum.photos/444/444/?random">
			</div>

			<!--► Product name ◄-->
			<h3 class="c-card--name">Pork</h3>

			<!--► Detail icon ◄--><a class="c-card--detail" href="product.html"><i class="fas fa-search"></i></a>

			<!--► Price ◄-->
			<span class="c-card--price">$40</span>

			<!--► Cart icon ◄-->
			<div class="c-card--cart"><i class="fas fa-shopping-cart"></i></div>

			<!--► Links ◄-->
			<a class="c-card--link" href="product.html"></a>

		</div><!-- ▲ c-card ▲ -->
		<!--────────────────────────────────-->


		<!--► Card ◄-->
		<!--────────────────────────────────-->
		<div class="c-card e-goto_url">

			<!--► Image ◄-->
			<div class="c-card--img">
				<img alt="" width="220" height="220" src="https://picsum.photos/445/445/?random">
			</div>

			<!--► Product name ◄-->
			<h3 class="c-card--name">Ham</h3>

			<!--► Detail icon ◄--><a class="c-card--detail" href="product.html"><i class="fas fa-search"></i></a>

			<!--► Price ◄-->
			<span class="c-card--price">$70</span>

			<!--► Cart icon ◄-->
			<div class="c-card--cart"><i class="fas fa-shopping-cart"></i></div>

			<!--► Links ◄-->
			<a class="c-card--link" href="product.html"></a>

		</div><!-- ▲ c-card ▲ -->
		<!--────────────────────────────────-->


		<!--► Card ◄-->
		<!--────────────────────────────────-->
		<div class="c-card e-goto_url">

			<!--► Image ◄-->
			<div class="c-card--img">
				<img alt="" width="220" height="220" src="https://picsum.photos/446/446/?random">
			</div>

			<!--► Product name ◄-->
			<h3 class="c-card--name">Jam</h3>

			<!--► Detail icon ◄--><a class="c-card--detail" href="product.html"><i class="fas fa-search"></i></a>

			<!--► Price ◄-->
			<span class="c-card--price">$40</span>

			<!--► Cart icon ◄-->
			<div class="c-card--cart"><i class="fas fa-shopping-cart"></i></div>

			<!--► Links ◄-->
			<a class="c-card--link" href="product.html"></a>

		</div><!-- ▲ c-card ▲ -->
		<!--────────────────────────────────-->


	</div><!-- ▲ .l-block_card ▲ -->
	<!--============================-->
</div><!-- ▲ l-container ▲ -->

</section><!-- !SECTION | S E C T I O N    1-->
<!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->
<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<!-- SECTION | S E C T I O N    2-->
<!---->
<!--		@sec2-->
<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<section class="p-index_2">
<div class="l-container">


	<!--► Title ◄-->
	<!--────────────────────────────────-->
	<h2 class="c-title">Lastest Blogs</h2>
	<!--────────────────────────────────-->


	<!--► Blog List ◄-->
	<!--────────────────────────────────-->
	<div class="l-block_card l-block_card--big">
	</div><!-- ▲ .l-block_card ▲ -->
	<!--────────────────────────────────-->
</div><!-- ▲ l-container ▲ -->

</section><!-- !SECTION | S E C T I O N    2-->
<!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->

<?php get_footer(); ?>