<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<!--► Search ◄-->
<!--────────────────────────────────-->
<form	class="c-search c-side" role="search" method="get"
		action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input	type="search" name="s" placeholder="Search..."
			id="<?php echo $unique_id; ?>"
			value="<?php echo get_search_query(); ?>">
	<input type="submit" value="">
	<i class="fas fa-search"></i>
</form><!-- ▲ c-search ▲ -->
<!--────────────────────────────────-->
