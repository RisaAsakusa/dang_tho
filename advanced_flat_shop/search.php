<?php get_header(); ?>

<!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->
<!---->
<!-- ANCHOR | M A I N-->
<!---->
<!--		@main-->
<!---->
<!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->

<main class="l-main p-blog">


<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<!-- SECTION | S E C T I O N    B A N N E R -->
<!---->
<!--		@secbanner    @banner -->
<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<section class="l-banner">

	<div class="c-banner">
		<h2>Banner</h2>
	</div>

</section><!-- !SECTION | S E C T I O N    B A N N E R -->
<!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->




<!--► Big Title ◄-->
<!--────────────────────────────────-->
<h2 class="c-title">Search</h2>
<!--────────────────────────────────-->


<!--► Breadcrumb ◄-->
<!--────────────────────────────────-->
<ul class="c-breadcrumb">
	<li><?php the_search_query(); ?></li>
</ul>
<!--────────────────────────────────-->








<div class="l-container">

<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<!-- SECTION | S E C T I O N    1-->
<!---->
<!--		@sec1-->
<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<section class="p-blog_1 l-container_side">
  <?php
	$search = get_search_query();
    echo do_shortcode('[loadPostsIsBeingSearched search="' . $search . '"]'); ?>
</section><!-- !SECTION | S E C T I O N    1-->

<!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->








<?php get_sidebar(); ?>








</div><!-- ▲ Container ▲ -->

<?php get_footer(); ?>