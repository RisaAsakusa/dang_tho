<?php get_header(); ?>

<!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->
<!---->
<!-- ANCHOR | M A I N-->
<!---->
<!--		@main-->
<!---->
<!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->

<main class="l-main p-blog">


<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<!-- SECTION | S E C T I O N    B A N N E R -->
<!---->
<!--		@secbanner    @banner -->
<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<section class="l-banner">

	<div class="c-banner">
		<h2>Banner</h2>
	</div>

</section><!-- !SECTION | S E C T I O N    B A N N E R -->
<!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->




<!--► Big Title ◄-->
<!--────────────────────────────────-->
<h2 class="c-title">Blog</h2>
<!--────────────────────────────────-->


<!--► Breadcrumb ◄-->
<!--────────────────────────────────-->
<ul class="c-breadcrumb e-goto_url">
	<li><a href="<?php echo get_site_url(); ?>">Home</a></li>
	<li>Blog</li>
</ul>
<!--────────────────────────────────-->








<div class="l-container">

<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<!-- SECTION | S E C T I O N    1-->
<!---->
<!--		@sec1-->
<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<section class="p-blog_1 l-container_side">
<?php if ( have_posts() ) : ?>


<!--============================-->
<div class="l-block_card">



<?php while ( have_posts() ) : the_post(); ?>

<!--► Card ◄-->
<!--────────────────────────────────-->
<div class="c-card e-goto_url">

	<!--► Image ◄-->
	<div class="c-card--img">
	<?php if ( has_post_thumbnail() ) {
		the_post_thumbnail();
		} else { ?>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/default.png" alt="<?php the_title(); ?>" />
	<?php } ?>
	</div>

	<!--► Post name ◄-->
	<h3 class="c-card--name"><?php echo get_the_title(); ?></h3>

	<!--► Date ◄-->
	<div class="c-card--date">Posted on<a href="<?php echo get_month_link('', ''); ?>"><?php echo get_the_date(); ?></a></div>

	<!--► Links ◄-->
	<a class="c-card--link" href="<?php echo get_permalink( $post->ID ); ?>"></a>

</div><!-- ▲ c-card ▲ -->
<!--────────────────────────────────-->

<?php endwhile; ?>



</div><!-- ▲ .l-block_card ▲ -->
<!--============================-->




<!--============================-->
<!--► Pagination ◄-->
<div class="c-pagination e-goto_url">

	<?php the_posts_pagination($posts_pagination);?>

</div><!-- ▲ .c-pagination ▲ -->
<!--============================-->

<?php else : ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>

<?php endif; ?>


</section><!-- !SECTION | S E C T I O N    1-->

<!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->








<?php get_sidebar(); ?>








</div><!-- ▲ Container ▲ -->

<?php get_footer(); ?>