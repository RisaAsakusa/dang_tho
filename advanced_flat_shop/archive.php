<?php get_header(); ?>

<!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->
<!---->
<!-- ANCHOR | M A I N-->
<!---->
<!--		@main-->
<!---->
<!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->

<main class="l-main p-blog">


	<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
	<!-- SECTION | S E C T I O N    B A N N E R -->
	<!---->
	<!--		@secbanner    @banner -->
	<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
	<section class="l-banner">

		<div class="c-banner">
			<h2>Banner</h2>
		</div>

	</section><!-- !SECTION | S E C T I O N    B A N N E R -->
	<!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->




	<!--► Big Title ◄-->
	<!--────────────────────────────────-->
	<h2 class="c-title">Archive</h2>
	<!--────────────────────────────────-->


	<!--► Breadcrumb ◄-->
	<!--────────────────────────────────-->
	<ul class="c-breadcrumb">
		<?php if ( is_category() ) : ?>
			<li><a>Category</a></li>
			<li><?php single_cat_title(); ?></li>
			<?php elseif ( is_tag() ) : ?>
			<li><a>Tag</a></li>
			<li><?php single_tag_title(); ?></li>
			<?php else : ?>
			<li><a>Archive</a></li>
			<li><?php the_archive_title(); ?></li>
		<?php endif; ?>
	</ul>
	<!--────────────────────────────────-->








	<div class="l-container">

		<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
		<!-- SECTION | S E C T I O N    1-->
		<!---->
		<!--		@sec1-->
		<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
		<section class="p-blog_1 l-container_side">
			<!--────────────────────────────────-->
			<?php
				$term = get_queried_object();
				echo do_shortcode('[loadPosts tax='
				.$term->taxonomy. ' name="'. $term->name .'"]');
			?>
			<!--────────────────────────────────-->
		</section><!-- !SECTION | S E C T I O N    1-->

		<!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->








		<?php get_sidebar(); ?>








	</div><!-- ▲ Container ▲ -->

<?php get_footer(); ?>