<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<!--████████████████████████████████████████████████████████████████-->

<head>

<!--► Title ◄-->
<title><?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>


<!--► Meta ◄-->
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Lorem ipsum">
<meta name="keywords" content="Lorem ipsum">


<!--► Favicon ◄-->
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/required.png" type="image/png">


<!--► CSS ◄-->
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/assets/css/swiper.min.css" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet">


<!--► JS ◄-->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/01_jquery-3.3.1.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/02_scrollbar.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/03_swiper.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/04_kinet.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/99_common.js"></script>
<?php if ( is_front_page() && is_home() ) : ?>
<?php elseif ( is_front_page() ) : ?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.script.js"></script>
<?php endif; ?>


<!-- ▼ Wordpress Head ▼ -->

<?php wp_head(); ?>

<!-- ▲ Wordpress Head ▲ -->

</head>

<!--████████████████████████████████████████████████████████████████-->

<body class="preload">
<!--► Effects ◄-->
<!--============================-->

<!--► Cursor Trail ◄-->
<div id="js-cur"></div>

<!--► Preload Effect ◄-->
<!--────────────────────────────────-->
<div class="e-load">
  <div><img alt="" width="198" height="198" draggable="false" src="<?php echo get_template_directory_uri(); ?>/assets/img/loading.gif"></div>
  <div>Loading...</div>
</div>

<!--============================-->

<?php if ( is_front_page() && is_home() ) : ?>
<?php elseif ( is_front_page() ) : ?>
<div class="l-wrapper p-index">
<?php elseif ( is_home() || is_single() || is_search() ||  is_archive() ) : ?>
<div class="l-wrapper l-yellow">
<?php else : ?>
<div class="l-wrapper">
<?php endif; ?>

<!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->
<!---->
<!-- ANCHOR | H E A D E R-->
<!---->
<!--    @header-->
<!---->
<!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->

<header class="l-header">


  <!--Logo-->
  <div class="c-txt_logo e-goto_url">
  <a href="<?php echo get_site_url(); ?>">Logo</a>
  </div>
  <!--────────────────────────────────-->


  <!--Global Navigation-->
  <!--────────────────────────────────-->
  <?php
    wp_nav_menu(array(
      'theme_location'  => 'primary-menu',
      'container_class'   => 'c-gnavi e-goto_url'
    ) );
  ?>
  <!--────────────────────────────────-->


</header><!-- ▲ Header ▲ -->
<!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->







