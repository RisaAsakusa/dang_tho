$(document).ready(function() {

//================================================================
// ANCHOR | P R E L O A D
//
//		@preload
//================================================================
//________________________________________________________

$(window).on('load', function() {
	//Loaded
	setTimeout(function(){
		$('body').removeClass('preload');
		$('body').addClass('loaded');
	}, 0);

	//Apply Effect when go to another URL
	$(".e-goto_url a").click(function(e) {
		e.preventDefault();
		var href = $(this).attr('href');
		$(".l-wrapper").fadeOut(200, function(){
			window.location = href;
		});
	});
})

window.onpopstate = function(e){
	if(e.state){
		document.getElementsByClassName(".l-wrapper").innerHTML = e.state.html;
		document.title = e.state.pageTitle;
	}
};

//________________________________________________________




//================================================================
// ANCHOR | S C R O L L    B A R
//
//		@scrollbar
//================================================================
//________________________________________________________

$("body").niceScroll({
	cursorwidth: "8px",
	autohidemode: "leave",
	//cursoropacitymin: 0.7,
	scrollspeed: 40,
	mousescrollstep: 50,
	sensitiverail: false,
});

//________________________________________________________




//================================================================
// ANCHOR | F O L L O W    C U R S O R
//
//		@followcursor    @cursorfollow
//================================================================
//________________________________________________________

// create instance of kinet with custom settings
var kinet = new Kinet({
	acceleration: 0.06,
	friction: 0.20,
	names: ["x", "y"],
});

// select circle element
var cursor = document.getElementById('js-cur');

// set handler on kinet tick event
kinet.on('tick', function(instances) {
	cursor.style.transform = `translate3d(${ (instances.x.current) }px, ${ (instances.y.current) }px, 0) rotateX(${ (instances.x.velocity/2) }deg) rotateY(${ (instances.y.velocity/2) }deg)`;
});

// call kinet animate method on mousemove
document.addEventListener('mousemove', function (event) {
	kinet.animate('x', event.clientX - window.innerWidth/2);
	kinet.animate('y', event.clientY - window.innerHeight/2);
});

//________________________________________________________




//================================================================
// ANCHOR | S C R O L L    T O    T O P
//
//		@scrolltop
//================================================================
//________________________________________________________

//Check to see if the window is top if not then display button
$(window).scroll(function(){
	if ($(this).scrollTop() > 220) {
		$('.l-btn_top').fadeIn(200);
	} else {
		$('.l-btn_top').fadeOut(200);
	}
});

//Click event to scroll to top
$('.c-btn_top').click(function(){
	$('html, body').animate({scrollTop : 0},400);
	return false;
});

//________________________________________________________




//================================================================
// ANCHOR | F I X E D    B U T T O N S
//
//		@fixedbtn    @fb
//================================================================
//________________________________________________________

function checkOffset() {

	if($('.l-btn_top').offset().top + $('.l-btn_top').height() >= $('.l-footer').offset().top - 10)
		$('.l-btn_top').css({"position": "absolute"});

	if($(document).scrollTop() + window.innerHeight < $('.l-footer').offset().top)
		$('.l-btn_top').css({"position": "fixed"});

	if($('.l-btn_shop').offset().top + $('.l-btn_shop').height() >= $('.l-footer').offset().top - 10)
		$('.l-btn_shop').css({"position": "absolute"});

	if($(document).scrollTop() + window.innerHeight < $('.l-footer').offset().top)
		$('.l-btn_shop').css({"position": "fixed"});
}

$(document).scroll(function() {
	checkOffset();
});

//________________________________________________________




//================================================================
// ANCHOR | G L O B A L    N A V I G A T I O N
//
//		@gnavi
//================================================================
//________________________________________________________

$(".c-gnavi a").click(function(e) {
	if ($(this).attr('href') != '' && $(this).attr('href') != '#' && $(this).attr('href') != undefined) {
		e.preventDefault();
		var href = $(this).attr('href');
		$(".l-wrapper").fadeOut(200, function(){
			window.location = href;
		});
	} else {
		e.preventDefault();
	}
});

//________________________________________________________





//================================================================
// ANCHOR | S T I C K Y    M E N U
//
//		@stickymenu    @sm
//================================================================
//________________________________________________________

//var sticky = header.offsetTop;

var l_banner = $('.l-banner');
var banner_pos = l_banner.position().top + l_banner.outerHeight(true) + 100;
var banner_pos_x = banner_pos - 200;

$(window).scroll(function(){
	if (window.pageYOffset > banner_pos) {
		$(".l-header").addClass("js-sticky");
		$(".l-header").removeClass("js-stable");
	} else {
		$(".l-header").removeClass("js-sticky");
	}

	if (window.pageYOffset > banner_pos_x && pageYOffset < banner_pos) {
		$(".l-header").addClass("js-stable");
	}
});

//________________________________________________________




//================================================================
// ANCHOR | C O M M E N T
//
//		@comment
//================================================================
//________________________________________________________

$('.c-show_comment').click(function (e) {
	e.preventDefault();
	if($('.l-comment').is(":visible")) {
		$('.l-comment').fadeOut();
		$('.c-post').delay(400).fadeToggle(function(){
			$('body').getNiceScroll().resize();
		});
	} else if($('.c-post').is(":visible")){
		$('.c-post').fadeOut();
		$('.l-comment').delay(400).fadeToggle(function(){
			$('body').getNiceScroll().resize();
		});
	}
});

$(".c-show_comment").click(function () {
	$(this).text(function(i, text){
		return text === "Show The Post >>" ? "Show Comments >>" : "Show The Post >>";
	})
 });

//________________________________________________________




//================================================================
// ANCHOR | T E X T A R E A    C O U N T
//
//		@txtcount
//================================================================
//________________________________________________________
$('.c-post_comment--area textarea').keyup(function() {
	var x = $(this).val().length;
	$('.c-post_comment--area div a span').text(' ' + x + ' ');
});


//________________________________________________________




//================================================================
// ANCHOR | P R O D U C T    S L I D E R
//
//		@pslider    @slider
//================================================================
//________________________________________________________

//	Create a Product Slider
var product = new Swiper('.c-product_slide--container', {
	effect: 'cube',
	//loop: true,
	grabCursor: true,
	pagination: {
		el: '.c-product_slide--page',
		clickable: true,
	},
});

//	Syn Image intoPage bullet
for(let i = 1; i < 1000; i++) {
	let getImageSrc = $('.c-product_slide--card:nth-child(' + i + ') img').attr('src');
	$('.c-product_slide .swiper-pagination-bullet:nth-child(' + i + ')').css('background-image', 'url(' + getImageSrc + ')');
}

//________________________________________________________




//================================================================
// ANCHOR | P R O D U C T    D E T A I L
//
//		@productdetail
//================================================================
//________________________________________________________

var product_info_1 = $('.c-product_detail--detail .c-product_detail--desc');

var product_info_2 = $('.c-product_detail--detail .c-product_detail--info');

var product_info_3 = $('.c-product_detail--detail .c-product_detail--reviews');

$('.c-product_detail--header a').click(function () {
	$(this).addClass('is-active');
	$('.c-product_detail--header a').not(this).removeClass('is-active');
});

$('.c-product_detail--header .c-product_detail--header_1').click(function () {
	product_info_1.delay(400).fadeIn(function(){
		$('body').getNiceScroll().resize();
	});
	product_info_2.fadeOut(function(){
		$('body').getNiceScroll().resize();
	});
	product_info_3.fadeOut(function(){
		$('body').getNiceScroll().resize();
	});
});

$('.c-product_detail--header .c-product_detail--header_2').click(function () {
	product_info_1.fadeOut(function(){
		$('body').getNiceScroll().resize();
	});
	product_info_2.delay(400).fadeIn(function(){
		$('body').getNiceScroll().resize();
	});
	product_info_3.fadeOut(function(){
		$('body').getNiceScroll().resize();
	});
});

$('.c-product_detail--header .c-product_detail--header_3').click(function () {
	product_info_1.fadeOut(function(){
		$('body').getNiceScroll().resize();
	});
	product_info_2.fadeOut(function(){
		$('body').getNiceScroll().resize();
	});
	product_info_3.delay(400).fadeIn(function(){
		$('body').getNiceScroll().resize();
	});
});

//________________________________________________________




//================================================================
// ANCHOR | T E X T A R E A    C O U N T
//
//		@textacount
//================================================================
//________________________________________________________

$('.c-form_input_address--textarea textarea').keyup(function() {
	var x = $(this).val().length;
	$('.c-form_input_address--textarea p span:nth-child(1)').text(x);
});

//________________________________________________________




//================================================================
// ANCHOR | P A Y M E N T    M E T H O D
//
//		@paymentmethod
//================================================================
//________________________________________________________

$('.c-payment_method .c-block_submit label').click(function () {
	$(this).parent().find('.c-block_submit--content').slideDown(function(){
		$('body').getNiceScroll().resize();
	});
	$('.c-payment_method .c-block_submit label').not(this).parent().find('.c-block_submit--content').slideUp(function(){
		$('body').getNiceScroll().resize();
	});
});

//________________________________________________________





}) // <-- document ready function