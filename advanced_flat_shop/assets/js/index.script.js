//================================================================
// ANCHOR | I N D E X    F U N C T I O N
//
//		@indexfunc
//================================================================
//________________________________________________________
$(document).ready(function() {
	var index_2_pos = $('.p-index_2').offset().top;
	var index_2_pos_x = index_2_pos - 220;

	$(window).scroll(function(){
		if (window.pageYOffset > index_2_pos_x) {
			$('.p-index').addClass('l-yellow');
		} else {
			$('.p-index').removeClass('l-yellow');
		}
	});
});

//________________________________________________________