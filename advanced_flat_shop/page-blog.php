<?php get_header(); ?>

    <!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->
    <!---->
    <!-- ANCHOR | M A I N-->
    <!---->
    <!--    @main-->
    <!---->
    <!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->

    <main class="l-main p-blog">


      <!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
      <!-- SECTION | S E C T I O N    B A N N E R -->
      <!---->
      <!--    @secbanner    @banner -->
      <!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
      <section class="l-banner">

        <div class="c-banner">
          <h2>Banner</h2>
        </div>

      </section><!-- !SECTION | S E C T I O N    B A N N E R -->
      <!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->

      <!--► Big Title ◄-->
      <!--────────────────────────────────-->
      <h2 class="c-title">Blog</h2>
      <!--────────────────────────────────-->


      <!--► Breadcrumb ◄-->
      <!--────────────────────────────────-->
      <ul class="c-breadcrumb e-goto_url">
        <li><a href="index.html">Home</a></li>
        <li><a>Blog</a></li>
      </ul>
      <!--────────────────────────────────-->

      <div class="l-container">


        <!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
        <!-- SECTION | S E C T I O N    1-->
        <!---->
        <!--    @sec1-->
        <!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
        <section class="p-blog_1 l-container_side">


          <!--────────────────────────────────-->
          <?php
            echo do_shortcode('[loadPostWhenScroll]');
          ?>
          <!--────────────────────────────────-->

        </section><!-- !SECTION | S E C T I O N    1-->
        <!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->
        <!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
        <!-- SECTION | S I D E B A R-->
        <!---->
        <!--    @sidebar-->
        <!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
        <div class="l-sidebar">


          <!--► Search ◄-->
          <!--────────────────────────────────-->
          <div class="c-search c-side">
            <input type="search" name="search" placeholder="Search...">
            <input type="submit" value="">
            <i class="fas fa-search"></i>
          </div><!-- ▲ c-search ▲ -->
          <!--────────────────────────────────-->


          <!--► List ◄-->
          <!--────────────────────────────────-->
          <div class="c-list e-goto_url c-side">

            <span>Categories</span>

            <ul>
              <li><a href="/">Category 1<span>(99)</span></a></li>
              <li><a href="/">Category 2<span>(99)</span></a></li>
              <li><a href="/">Category 3<span>(99)</span></a></li>
              <li><a href="/">Category 4<span>(99)</span></a></li>
              <li><a href="/">Category 5<span>(99)</span></a></li>
              <li><a href="/">Category 6<span>(99)</span></a></li>
            </ul>
          </div><!-- ▲ c-list ▲ -->
          <!--────────────────────────────────-->


        </div><!-- !SECTION | S I D E B A R-->
        <!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->

      </div><!-- ▲ Container ▲ -->

      <!-- @scrolltop-->
      <!--────────────────────────────────-->
      <div class="l-btn_top">
        <div class="c-btn_top"></div>
      </div>
      <!--────────────────────────────────-->


      <!-- @shopbtn-->
      <!--────────────────────────────────-->
      <div class="l-btn_shop e-goto_url">
        <a class="c-btn_shop" href="cart.html">
          <i class="fas fa-shopping-cart"></i>
          <span>99</span>
        </a></div>
      <!--────────────────────────────────-->
    </main><!-- ▲ Main ▲ -->
    <!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->

<?php get_footer(); ?>
