<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<div <?php wc_product_class("c-card e-goto_url"); ?>>

	<!--► Image ◄-->
	<div class="c-card--img">
		<?php echo woocommerce_get_product_thumbnail(); ?>
	</div>

	<!--► Product name ◄-->
	<h3 class="c-card--name">
		<?php the_title(); ?>
	</h3>

	<!--► Detail icon ◄-->
	<a class="c-card--detail" href="<?php echo get_permalink($product_id); ?>"><i class="fas fa-search"></i></a>
	<?php if ( $product->is_on_sale() ) : ?>

	<!--► Sale ◄-->
	<div class="c-card--sale">Sale !</div>
	<?php endif; ?>

	<!--► Price ◄-->
	<span class="c-card--price">
		<?php echo woocommerce_template_loop_price(); ?>
	</span>

	<!--► Cart icon ◄-->
	<div class="c-card--cart">
		<?php echo woocommerce_template_loop_add_to_cart(); ?>
		<i class="fas fa-shopping-cart"></i>
	</div>

	<!--► Links ◄-->
	<a class="c-card--link" href="<?php echo get_permalink($product_id); ?>"></a>


</div>
