/*
*@Title: Load posts when scrolling the scrollbar
*@reference: https://makitweb.com/load-content-on-page-scroll-with-jquery-and-ajax/#jquery
*/

/*---------------------------------------*/

(function getPostsWhenScrolling(){
  $container = $(".l-block_card_js"); //box contain posts loaded by JAJAX;
  var posts = 0;

  //check if class $container is exist
  if ($container.length > 0) {
    var parameter = {
      tax: $container.data("tax"),
      name: $container.data("name")
    };
    //Get post when scrolling to bottom of layout
    loadPostsWhenScrolling(posts, $container, parameter, "do_filter_posts");
  }
})();

/*
*@action: When user searching any character in searchform,
 browser will load posts evreytime use scrolling the page
 (use AJAX to load)
*/
(function getPostsWhichSearched(){
  $container = $(".search-posts_js"); //box contain posts loaded by JAJAX;
  var posts = 0;

  //check if class $container is exist
  if ($container.length > 0) {
    var parameter = {
      search: $container.data("search")
    };
    //Get post when scrolling to bottom of layout
    loadPostsWhenScrolling(posts, $container, parameter, "do_filter_search_posts");
  }
})();

/*-------------------------------*/

function loadPostsWhenScrolling(posts = 0, container, parameter, action) {
  $(window).scroll(function(event) {
    /*---------VARIABLE----------*/
    var position = $(window).scrollTop();
    var bottom = $(document).height() - $(window).height();
    var postsperrow = 3; //point 3 posts 1 row
    /*---------------------------*/

    //if you scroll to end of the page, load posts
    if (position == bottom) {
      posts = postsperrow + posts;
      parameter["posts"] = posts;

      $.ajax({
        url: bobaz.ajax_url,
        data: {
          action: action,
          //nonce use for verifying when send data to server
          nonce: bobaz.nonce,
          parameter: parameter
        },
        type: 'GET',
        dataType: 'json'
      })
      .done(function(data) {
        container.html(data.content);
        $("body").getNiceScroll().resize();
      })
      .fail(function(err) {
        console.log(err);
      });
    }
  });
};
