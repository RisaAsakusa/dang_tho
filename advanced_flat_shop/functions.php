<?php
include("load_ajax/loadpost_ajax.php");
//================================================================
// ANCHOR | Post Thumbnails
//
//		@thumbnails
//================================================================
//________________________________________________________

add_theme_support( 'post-thumbnails' );

//________________________________________________________
if(!function_exists("add_ajax_to_js")) {
  function add_ajax_to_js() {
    wp_enqueue_script('ajax', get_template_directory_uri().'/js/ajax_loadpost.js', ['jquery'], null, true);
    wp_localize_script('ajax', 'bobaz', array(
      'nonce' => wp_create_nonce( 'bobaz' ),
      'ajax_url' => admin_url('admin-ajax.php')
    ));
  }
  add_action('wp_enqueue_scripts', 'add_ajax_to_js');
}
//________________________________________________________

//================================================================
// ANCHOR | N A V I G A T I O N
//
//		@navi
//================================================================
//________________________________________________________

function flat_gnavi() {
	register_nav_menus(
		array(
			'primary-menu'	=> __('Flat Main Menu', 'advanced_flat_shop')
		)
	);
}
add_action( 'init', 'flat_gnavi' );

//________________________________________________________




//================================================================
// ANCHOR | P A G I N A T I O N
//
//		@pagination
//================================================================
//________________________________________________________

$posts_pagination = array(
	//'base'               => '%_%',
	//'format'             => '?paged=%#%',
	//'total'              => 1,
	//'current'            => 0,
	'show_all'           => false,
	'end_size'           => 1,
	'mid_size'           => 2,
	'prev_next'          => true,
	'prev_text'          => '',
	'next_text'          => '',
	'type'               => 'plain',
	'add_args'           => false,
	'add_fragment'       => '',
	'before_page_number' => '',
	'after_page_number'  => ''
);

//________________________________________________________




//================================================================
// ANCHOR | C A T E G O R Y
//
//		@categories
//================================================================
//________________________________________________________

add_filter( 'wp_list_categories', 'x_category_list' );
function x_category_list( $list ) {
	//move count inside a tags
	$list = str_replace( '</a> (', '<span>(', $list );
	$list = str_replace( ')', ')</span></a>', $list );
	return $list;
}

//________________________________________________________




//================================================================
// ANCHOR | C O M M E N T
//
//		@comment
//================================================================
//________________________________________________________

function x_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	?>
	<?php if ( $comment->comment_approved == '1' ): ?>
	<!--► Comment ◄-->
	<!--────────────────────────────────-->
	<div class="c-comment">

		<div class="c-comment--row_1">
			<div class="c-comment--img">
				<?php echo '<img alt="" src="'.get_avatar_url($comment).'" width="64" height="64">' ?>
			</div>
			<div class="c-comment--area">
				<?php comment_text() ?>
			</div>
		</div>

		<div class="c-comment--row_2">
			<div>
				<div class="c-comment--author">
				<?php echo get_comment_author() ?>
				</div>
				<a class="c-comment--date">
				<?php
					$d = "l, F jS, Y";
					$comment_date = get_comment_date( $d, $comment_ID );
					echo $comment_date;
				?>
				</a>
			</div>
			<div>
				<?php comment_reply_link(array_merge(
					$args, array(
						'reply_text' => 'Reply',
						'depth' => $depth,
						'max_depth' => $args['max_depth']
					)
				))?>
			</div>
		</div>

	</div><!-- ▲ Comment ▲ -->
	<!--────────────────────────────────-->
	<?php endif;
}

//________________________________________________________

//	Remove Website input
add_filter('comment_form_default_fields', 'website_remove');
function website_remove($fields) {
	if(isset($fields['url']))
	unset($fields['url']);
	return $fields;
}

//	Wrap fields
function x_comment_form_before_fields() {
	echo '<div class="c-comment_auth">';
}
add_action('comment_form_before_fields', 'x_comment_form_before_fields');

function x_comment_form_after_fields() {
	echo '</div>';
}
add_action('comment_form_after_fields', 'x_comment_form_after_fields');

//________________________________________________________

//	Add class for comment_reply_link
add_filter('comment_reply_link', 'replace_reply_link_class');
function replace_reply_link_class($class){
	$class = str_replace("class='comment-reply-link", "class='c-comment--reply", $class);
	return $class;
}

//________________________________________________________

?>