<?php
/*
* Template Name: Full
* Template Post Type: post
*/
get_header(); ?>

<?php while ( have_posts() ) : the_post();?>

<!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->
<!---->
<!-- ANCHOR | M A I N-->
<!---->
<!--		@main-->
<!---->
<!--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■-->

<main class="l-main p-post">


<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<!-- SECTION | S E C T I O N    B A N N E R -->
<!---->
<!--		@secbanner    @banner -->
<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<section class="l-banner">

<?php if (has_post_thumbnail( $post->ID ) ): ?>

<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
	<div class="c-banner" style="background-image: url('<?php echo $image[0]; ?>')"></div>

<?php else : ?>

	<div class="c-banner" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/default.png')"></div>

<?php endif; ?>

</section><!-- !SECTION | S E C T I O N    B A N N E R -->
<!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->




<!--► Big Title ◄-->
<!--────────────────────────────────-->
<h2 class="c-title"><?php the_title(); ?></h2>
<!--────────────────────────────────-->


<!--► Breadcrumb ◄-->
<!--────────────────────────────────-->
<ul class="c-breadcrumb e-goto_url">
	<li><a href="<?php echo get_site_url(); ?>">Home</a></li>
	<li><a href="/blog">Blog</a></li>
	<li>Post</li>
</ul>
<!--────────────────────────────────-->


<hr class="c-misc_hr">


<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<!-- SECTION | S E C T I O N    1-->
<!---->
<!--		@sec1-->
<!--▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼▼-->
<section class="p-post_1">
<div class="l-container">


<?php get_sidebar(); ?>




<div class="l-container_side">

<!--► Post Content ◄-->
<!--============================-->
<div class="c-post">

	<?php the_content(); ?>

</div>
<!--============================-->
<!-- ▲ Post Content ▲ -->




<?php
	if ( comments_open() || get_comments_number() ) :
			comments_template();
	endif;
?>




</div><!-- ▲ l-container_side ▲ -->

</div><!-- ▲ l-container ▲ -->

</section><!-- !SECTION | S E C T I O N    1-->
<!--▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲▲-->
<?php endwhile; ?>

<?php get_footer(); ?>