<?php
/**
* The template for dispaying post box format
*/
?>

<div class="c-card e-goto_url c-card-effect">
  <!--► Image ◄-->
  <div class="c-card--img">
    <?php if ( has_post_thumbnail() ) {
      the_post_thumbnail();
      } else { ?>
      <img alt="" width="220" height="220" src="https://picsum.photos/449/449/?random" />
    <?php } ?>
  </div>

  <!--► Product name ◄-->
  <h3 class="c-card--name"><?php echo get_the_title(); ?></h3>

  <!--► Date ◄-->
  <div class="c-card--date">Posted on<a href="<?php echo get_month_link('', ''); ?>"><?php echo get_the_date(); ?></a></div>

  <!--► Links ◄-->
  <a class="c-card--link" href="<?php echo get_permalink( $post->ID ); ?>"></a>

</div><!-- ▲ c-card ▲ -->