<?php get_header(); ?>

<main class="p-single">
  <div class="c-title c-title--page">
    <h1>TOPICS</h1>
  </div>
    <div class="l-container">
      <div class="l-sidebar">
        <?php dynamic_sidebar('left-sidebar'); ?>
      </div>
      <div class="l-main">
        <?php if(have_posts()): ?>
        <?php while(have_posts()) : the_post(); ?>
          <h2 class="single_title"><?php the_title(); ?></h2>
          <div class="single_info">
            <span><?php echo get_the_date(" Y/m/d "); ?></span>
            <a href="cat.html"><?php echo get_the_category()[0]->name; ?></a>
          </div>

          <div class="single_content">
              <?php
                // Post thumbnail.
                the_post_thumbnail('full', array('class' => 'img-fluid rounded'));
              ?>
              <?php the_content(); ?>
          </div>
        <?php endwhile; ?>
        <?php else: ?>
          <h1>Cand find post!</h1>
        <?php endif; ?>
      </div><!-- end l-main -->
    </div><!-- end l-container -->
</main>

<?php get_footer(); ?>
