<?php
/*
Template Name: Archives
*/
get_header()
?>
<main class="p-topics">
  <div class="c-title c-title--page">
    <h1>TOPICS</h1>
  </div><!-- end c-title -->
  <div class="l-container">
    <?php
      $query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'paged' => get_query_var( 'paged' )));
      if($query->have_posts()): ?>
        <ul class="p-toppics__list">
          <?php while($query->have_posts()) : $query->the_post(); ?>
          <li>
              <span class="datepost"><?php echo get_the_date(" Y/m/d "); ?></span>
              <a class="cat" href="<?php addLinkCategory_dangtho(get_the_category()[0]->name); ?>" title=""><?php echo get_the_category()[0]->name; ?></a>
              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </li>
          <?php endwhile; ?>
        </ul>

        <div class="c-pnav">
        <?php
          $html = paginate_links( array(
            'paged' => ( get_query_var('page') ) ? absint(get_query_var('page')) : 1,
            'total' => $query->max_num_pages,
            'show_all' => true,
            'prev_next' => true,
            'prev_text' => __("«"),
            'next_text' => __("»")
          ));
          //set your additional decorative elements
          //mimics the default for paginate_links()
          $pretext = '«';
          $posttext = '»';
          //assuming this set of links goes at bottom of page
          $pre_deco = '<a class="prev btn-pagi--mr1 page-numbers" href="" >' . $pretext . '</a>';
          $post_deco = '<a class="next btn-pagi--ml1 page-numbers" href="" >' . $posttext . '</a>';
           //key variable
          $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
          //add decorative non-link to first page
          if ( 1 === $paged) {
            $html = $pre_deco . $html;
          }
          //add decorative non-link to last page
          if ( $wp_query->max_num_pages == $paged ) {
            $html = $html . $post_deco;
          }
          echo $html;
          wp_reset_postdata();
          ?>
        </div><!-- end c-nav -->
      <?php else: ?>
      <?php _e('Sorry'); ?>
      <?php endif;?>
    <!-- <?php //pagination_nav(); ?> -->

  </div>
</main>
<?php get_footer() ?>
