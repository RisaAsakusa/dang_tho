<?php get_header(); ?>
<div class="mainvisual">
  <div class="l-container">
    <div class="l-slider">
      <a href="#"><img src="<?php echo get_template_directory_uri().'/img/main_img01_no.png' ?>" alt=""></a>
      <a href="#"><img src="<?php echo get_template_directory_uri().'/img/main_img02_no.png' ?>" alt=""></a>
      <a href="service.html"><img src="<?php echo get_template_directory_uri().'/img/main_img03_no.png' ?>" alt=""></a>
      <a href="#"><img src="<?php echo get_template_directory_uri().'/img/main_img04_no.png' ?>" alt=""></a>
    </div>
  </div>
</div>

<main>
  <div class="l-container">
    <div class="c-grouplink c-flex">
      <a href="#">
        <img src="<?php echo get_template_directory_uri().'/img/img_01_no.png' ?>" alt="" class="imglink">
      </a>
      <a href="#">
        <img src="<?php echo get_template_directory_uri().'/img/img_02_no.png' ?>" alt="" class="imglink">
      </a>
      <a href="#">
        <img src="<?php echo get_template_directory_uri().'/img/img_03_no.png' ?>" alt="" class="imglink">
      </a>
    </div>

    <div class="p-toppics">
      <h2 class="c-title">Topics</h2>
      <?php
        // the query
        $wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>5)); ?>
        <?php if ( $wpb_all_query->have_posts() ) : ?>
        <ul class="p-toppics__list">
          <!-- the loop -->
          <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
              <li>
                <span class="datepost"><?php echo get_the_date(" Y/m/d "); ?></span>
                <a class="cat" href="<?php addLinkCategory_dangtho(get_the_category()[0]->name); ?>" title=""><?php echo get_the_category()[0]->name; ?></a>
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
              </li><!-- <?php //echo esc_url( get_category_link(get_cat_ID(get_the_category()[0]->name)) ); ?> -->
          <?php endwhile; ?>
          <!-- end of the loop -->
        </ul>
        <?php wp_reset_postdata(); ?>
        <?php else : ?>
          <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
        <?php endif; ?>

      <div class="l-btn">
        <a href="<?php the_permalink(177); ?>" class="c-btn c-btn--small">一覧を見る</a>
      </div>
    </div>

    <div class="c-grouplink c-grouplink--2">
      <a href="#">
        <img src="<?php echo get_template_directory_uri().'/img/btn_03_no.png' ?>" alt="" class="imglink">
      </a>
      <a href="#">
        <img src="<?php echo get_template_directory_uri().'/img/btn_04_no.png' ?>" alt="" class="imglink">
      </a>
    </div>
    <div class="c-access">
      <div class="c-access__inner c-flex">
        <div class="c-access__items">
          <h3 class="c-title c-title--sub"><?php echo get_field('title_map', 15); ?></h3>
          <p class="address"><?php echo get_field('address', 15); ?></p>
          <p class="time"><?php echo get_field('time', 15); ?></p>
          <br/>
          <p>
            <span class="tel"><?php echo get_field('tel', 15); ?></span>
            <span class="fax"><?php echo get_field('fax', 15); ?></span>
            <br/>
            <span class="email"><?php echo get_field('email', 15); ?></span>
          </p>
        </div>
        <div class="c-access__items">
          <img src="<?php echo get_field('image_map', 15); ?>" alt="">
        </div>
      </div><!-- end c-access__inner -->
      <div class="c-access__inner c-flex">
        <div class="c-access__items">
          <h3 class="c-title c-title--sub"><?php echo get_field('title_map', 13); ?></h3>
          <p class="address"><?php echo get_field('address', 13); ?></p>
          <p class="time"><?php echo get_field('time', 13); ?></p>
          <br/>
          <p>
            <span class="tel"><?php echo get_field('tel', 13); ?></span>
            <span class="fax"><?php echo get_field('fax', 13); ?></span>
            <br/>
            <span class="email"><?php echo get_field('email', 13); ?></span>
          </p>
        </div>
        <div class="c-access__items">
          <img src="<?php echo get_field('image_map', 13); ?>" alt="">
        </div>
      </div><!-- end c-access__inner -->
    </div>
  </div>
</main>
<?php get_footer(); ?>
