<!DOCTYPE html>
<html <?php language_attributes( ); ?>>

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php bloginfo( 'title' ); ?></title>
    <!-- <link href="<?php //bloginfo('stylesheet_url'); ?>" rel="stylesheet"> -->
    <link href="<?php if (is_singular('post')) {
      echo get_template_directory_uri(). '/css/csswp/blog-post.css';}
      else echo get_template_directory_uri(). '/css/csswp/blog-home.css'; ?>" rel="stylesheet">
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' );  ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
          <?php wp_nav_menu(array('container_class' => "collapse navbar-collapse",
          "container_id" => "navbarResponsive",
          'menu_class' => 'navbar-nav ml-auto')); ?>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <!-- Blog Entries Column -->
        <div class="col-md-8">