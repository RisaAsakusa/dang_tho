
/* スマートロールオーバー */
function smartRollover(){if(document.getElementsByTagName)for(var b=document.getElementsByTagName("img"),a=0;a<b.length;a++)b[a].getAttribute("src").match("_out.")&&(b[a].onmouseover=function(){this.setAttribute("src",this.getAttribute("src").replace("_out.","_over."))},b[a].onmouseout=function(){this.setAttribute("src",this.getAttribute("src").replace("_over.","_out."))})}window.addEventListener?window.addEventListener("load",smartRollover,!1):window.attachEvent&&window.attachEvent("onload",smartRollover);

/*--------------------------------------------------------------------------*
 *
 *  SmoothScroll JavaScript Library V2
 *
 *  MIT-style license.
 *
 *  2007-2011 Kazuma Nishihata
 *  http://www.to-r.net
 *
 *--------------------------------------------------------------------------*/
new function(){function f(a){function d(a,c,b){setTimeout(function(){"up"==b&&a>=c?(a=a-(a-c)/20-1,window.scrollTo(0,a),d(a,c,b)):"down"==b&&a<=c?(a=a+(c-a)/20+1,window.scrollTo(0,a),d(a,c,b)):scrollTo(0,c)},10)}if(b.getElementById(a.href.replace(/.*\#/,""))){a=b.getElementById(a.href.replace(/.*\#/,"")).offsetTop;var c=b.documentElement.scrollHeight,e=window.innerHeight||b.documentElement.clientHeight;c-e<a&&(a=c-e);c=window.pageYOffset||b.documentElement.scrollTop||b.body.scrollTop||0;d(c,a,a<c?
"up":"down")}}var g=/noSmooth/,b=document;(function(a,d,c){try{a.addEventListener(d,c,!1)}catch(b){a.attachEvent("on"+d,function(){c.apply(a,arguments)})}})(window,"load",function(){for(var a=$("a[href*='#']").not(".noscrl"),b=0,c=a.length;b<c;b++)g.test(a[b].getAttribute("data-tor-smoothScroll"))||a[b].href.replace(/\#[a-zA-Z0-9_]+/,"")!=location.href.replace(/\#[a-zA-Z0-9_]+/,"")||(a[b].onclick=function(){f(this);return!1})})};
// noscrlというクラスを付けることでスムーススクロールの対象外にできます。（<a href="#header" class="noscrl">hoge</a>）

//===================================================
// SP menu
//===================================================
$(function() {

  $(".c-menuSP-js").on('click', function() {
    $(this).toggleClass("is-open");
    $('.c-menulistSP').slideToggle(400);
  });
});

//===================================================
// SP scrolltop
//===================================================
$(".c-f-btn2-js").click(function() {
  $("html, body").animate({ scrollTop: 0 }, 1000);
  return false;
});

//===================================================
// slider(using library slick.js)
//===================================================
$(document).ready(function() {
  //slider in the top page
  $('.c-slider1_js').slick({
    infinite: true,
    autoplay: true,
    autoplaySpeed: 3000,
    dots: true,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev"><img src="assets/img/prev.png" alt="prev.png" /></button>',
    nextArrow: '<button type="button" class="slick-next"><img src="assets/img/next.png" alt="next.png" /></button>'
  });

  //slides in Our Brand component
  $(".top4__slider1_js").slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    variableWidth: true,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-angle-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fas fa-angle-right"></i></button>',    
    responsive: [
      {
        breakpoint: 767,
        settings: {
          infinite: true,
          variableWidth: false,
          slidesToShow:3
        }
      },
      {
        breakpoint: 600,
        settings: {
          infinite: true,
          variableWidth: false,
          slidesToShow:2
        }
      },
      {
        breakpoint: 500,
        settings: {
          infinite: true,
          variableWidth: false,
          slidesToShow:1
        }
      },
    ]
  });
});

//===================================================
// toggle list in the side bar
//===================================================
$(".box1-toggle_js").click(function() {
  var box1 = $(this).next(".box1-content_js");
  var icon = $(this).find(".fa-chevron-down");

  if (box1.is(":hidden")) {
    box1.slideDown();
    icon.hide();
  } else {
    box1.slideUp();
    icon.show();
  }
});

//===================================================
// show tab at footer when the page is being showed 
//on SP screen
//===================================================
//1.slide up tab in footer on Smart phone
(function slideUpTabFooter() {
  hideTabFooter();

  $(window).resize(function() {
    if($(window).width() <= 767) {
      hideTabFooter();
    } else {
      $(".f-card1_js").each(function(index){
        var tab = $(this).next(".f-content_js");
        $(tab).show();
      });
    }
  });
})();

//2.toggle tab
$(".f-card1_js").click(function() {
  var icon = $(this).find(".f-plus_js");

  if (!$(icon).is(":hidden")) {
    var tab = $(this).next(".f-content_js");
    $(tab).slideToggle(200);
    icon.toggleClass("is-open");
  }
});

/*----------------------------------------
* hide tab in footer
----------------------------------------*/
function hideTabFooter() {
  $(".f-card1_js").each(function(index){
    var icon = $(this).find(".f-plus_js");

    if (!$(icon).is(":hidden") && !$(icon).hasClass("is-open")) {
      var tab = $(this).next(".f-content_js");
      $(tab).hide();
    }
  });
}

//===================================================
// show/ hide search form in SP creen
//===================================================
$(".btn-search1_js").click(function (e) {
  $(".form-search1_js").slideToggle(200);
});

//===================================================
// show more items in list item 1
//===================================================
$(".btn-show_js").click(function(e) {
  e.preventDefault();

  let listItems = $(".c-list1_js .c-list1__card1");

  listItems.each(function(index) {
    if ($(this).hasClass("hidden")) {
      $(this).fadeIn(500);
    }
  });

  $(this).hide(300);
});
